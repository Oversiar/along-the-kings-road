import Vue from 'vue'
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/scss/custom.scss'

import Vuex from 'vuex'

import App from './App.vue'
import VueTypedJs from 'vue-typed-js'

Vue.use(VueTypedJs);
Vue.use(Vuex);

const store = new Vuex.Store({
    state:{
        gamePhase: "start"
    },
    mutations:{
        increment (state){
            if(state.gamePhase === "start"){
                state.gamePhase = "battle";
            }
            else if(state.gamePhase === "battle"){
                state.gamePhase = "caravan";
            }
        }
    }
})

new Vue(
    {
        el: '#app',
        store: store,
        render(h) {
            return h(App)
        }
    })