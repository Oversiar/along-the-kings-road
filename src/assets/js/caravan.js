import Storage from './storage.js';

let caravan = null;

let caravanFunctions = {
    initializeCaravan(caravanIn) {
        caravan = caravanIn;
    }
}

export default caravanFunctions;

function randomNumber(max) {
    return Math.floor(Math.random() * max) + 1;
}