import PlayerInit from '../json/PlayerInit.json';
import CaravanInit from '../json/PlayerInit.json';
import Enemies from '../json/Enemies.json';

let items = [
    {level: 0, type: "item", name: "organ", material: "organ"},
    {level: 0, type: "item", name: "bone", material: "bone"},
    {level: 0, type: "item", name: "hide", material: "hide"},
    {
        level: 0,
        type: "weapon",
        name: "Stumps",
        attack: {choiceChance: 10, name: "Stumps", damage: 5, critChance: 10, critDamage: 10}
    },
    {
        level: 0,
        type: "shield",
        name: "Wooden Shield",
        hitLocation: {
            choiceChance: 10,
            name: 'Wooden Shield',
            alternateName: '',
            critsPossible: 1,
            critCounter: 0,
            critMessages: ['The shield was thrown aside'],
            critOutcome: {type: 'dropItem', value: 'this'},
            damageModifier: 50
        },
    }
];

let storage = {
    getPlayer() {
        return PlayerInit;
    },
    getCaravan(){
        return CaravanInit;
    },
    getGoblinKing() {
        return Enemies[0];
    },
    getEnemyByName(name) {
        return Enemies.find(enemy => enemy.name === name);
    },
    getItemByName(name) {
        return getItemByName(name);
    },
    getRandomMaterial(character){
        getRandomMaterial(character);
    }
};

export default storage;

function getItemByName(name) {
    return items.find(item => item.name === name);
}

function getRandomMaterial(character){
  let searchResult = items.find(item => item.type === "item" && (item.level === character.level || item.owner === character.name));

  return searchResult[randomNumber(searchResult.length) - 1];
}

function randomNumber(max) {
    return Math.floor(Math.random() * max) + 1;
}