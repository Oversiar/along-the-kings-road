import Storage from './storage.js';

const newline = '<br>';
let battle = null;

let battleFunctions = {
    activateAttack(player, attack, enemy) {
        resolveEnemyDefense(player, attack, enemy);
        nextTurn();
    },
    activateDefense(player, defense, enemy) {
        resolveEnemyAttack(player, defense, enemy)
        nextTurn();
    },
    initializeBattle(battleIn) {
        battle = battleIn;
    }
}

export default battleFunctions;

function resolveEnemyDefense(player, attack, enemy) {
    let defense = enemy.defenses[makeAiListChoice(enemy.defenses)];

    if (attack.damage !== undefined) {
        resolveAttack(player, attack, enemy, defense);
    } else {
        resolveSpecialAttack(player, attack, enemy, defense);
    }
}

function resolveEnemyAttack(player, defense, enemy) {
    let allAttacks = [];
    if (enemy.specialAttacks === undefined) {
        allAttacks = enemy.attacks;
    } else {
        allAttacks = enemy.attacks.concat(enemy.specialAttacks);
    }

    let attack = allAttacks[makeAiListChoice(allAttacks)];

    if (attack.damage !== undefined) {
        resolveAttack(enemy, attack, player, defense);
    } else {
        resolveSpecialAttack(enemy, attack, player, defense);
    }
}

function resolveAttack(attacker, attack, defender, defense) {
    let outputText = "";

    outputText += attacker.name + " attacked with " + attack.name

    let damage = attack.damage;

    let isCriticalHit = rollChance(attack.critChance + attacker.stats.luck);
    let isDefenseSuccessful = testDefenseSuccessful(defense, attacker, defender);

    let hitLocation = defender.hitLocations[makeAiListChoice(defender.hitLocations)];
    if(hitLocation.armour !== null){
        hitLocation = hitLocation.armour;
    }
    let hitLocationName = hitLocation.critCounter < hitLocation.critsPossible ? hitLocation.name : hitLocation.alternateName;

    if (isCriticalHit) {
        damage = attack.critDamage;
    }

    damage = applyStrengthModifier(damage, attacker.stats.strength);

    if (isDefenseSuccessful) {
        if (defense.reflect !== undefined) {
            resolveDamageTaken(attacker, applyDamageModifier(damage, defense.reflect))
        }
        damage = applyDamageModifier(damage, defense.damageModifier);
        outputText += newline + defender.name + " " + defense.pastTense;
        outputText += defense.reflect !== undefined ? " and reflected damage!" : "";
    }

    if (damage > 0) {
        outputText += newline + defender.name + " is hit in " + defender.pronoun + " ";
        outputText += hitLocationName;
        damage = applyDamageModifier(damage, hitLocation.damageModifier);
        if (isCriticalHit) {
            outputText += newline + "Critical Hit!";
            if (hitLocation.critCounter < hitLocation.critsPossible) {
                outputText += newline + hitLocation.critMessages[hitLocation.critCounter];
                hitLocation.critCounter++;
            } else {
                damage = applyDamageModifier(damage, 110);
                hitLocation.critCounter++;
            }
            if (hitLocation.critCounter === hitLocation.critsPossible) {
                outputText += resolveCritOutcomes(defender, hitLocation);
                if (hitLocation.alternateName === "") {
                    let hitLocationIndex = defender.hitLocations.indexOf(hitLocation);
                    defender.hitLocations.splice(hitLocationIndex, 1);
                }
                else if (hitLocation.alternateName === "break") {
                    console.log();
                    let armourLocation = defender.hitLocations.find(location => location.name === hitLocation.location);
                    armourLocation.armour = null;

                }
            }
        }
    }

    battle.outputText = outputText;
    resolveDamageTaken(defender, damage)
}

function resolveSpecialAttack(attacker, specialAttack, defender, defense) {
    let outputText = "";

    outputText += specialAttack.attackMessage;

    let outcome = specialAttack.effects[makeAiListChoice(specialAttack.effects)];

    if (outcome.type !== "fail") {
        switch (outcome.type) {
            case "spawnEnemy":
                for (let i = 0; i < outcome.details.value; i++) {
                    spawnEnemy(outcome.details.type);
                }
                break;
        }
    }
    outputText += newline + outcome.message;

    battle.outputText = outputText;
}

function spawnEnemy(name) {
    let enemy = JSON.parse(JSON.stringify(Storage.getEnemyByName(name)));
    battle.characters.push(enemy);
}

function resolveDamageTaken(character, damage) {
    character.health -= damage;
    if (character.health <= 0) {
        character.health = 0;
        resolveDeath(character);
    }
}

function resolveDeath(character) {
    battle.outputText += newline + character.name + " has been defeated."
    if (character.type === "player") {
        battle.outputText += newline + "Game Over.";
    } else if (character.type === "boss") {
        battle.outputText += newline + "Congrats!";
    } else if (character.type === "minion") {
        let enemyIndex = battle.characters.indexOf(character);
        battle.characters.splice(enemyIndex, 1);
        battle.target = 0;
    }
}

function resolveCritOutcomes(defender, hitLocation) {
    let critOutcome = hitLocation.critOutcome;
    let value = critOutcome.value;

    let outputText = "";

    switch (critOutcome.type) {
        case "damagePerTurn":
            defender.statusEffects.damagePerTurns.push(value);
            break;
        case "instantKill":
            defender.health = 0;
            break;
        case "lostHands":
            defender.attacks = [Storage.getItemByName("Stumps").attack];
            break;
        case "dropItem":
            if (value === "this") {
                battle.floorInventory.push(Storage.getItemByName(hitLocation.name));
            } else {
                let randomMaterial = Storage.getRandomMaterial(defender)
                battle.floorInventory.push(randomMaterial);
                outputText = newline + defender.name + " dropped a " + randomMaterial.name;
            }
            break;
        case "lostSpeed":
            defender.stats.speed -= value;
            break;
        case "lostStrength":
            defender.stats.strength -= value;
            break;
        case "lostAccuracy":
            defender.stats.accuracy -= value;
            break;
        case "knockedDown":
            defender.statusEffects.knockedDown = true;
            break;
    }

    return outputText;
}

function applyStrengthModifier(damage, value) {
    return damage * (100 + value * 10) / 100
}

function testDefenseSuccessful(defense, attacker, defender) {
    if (defense.name === "Block") {
        return rollChance(defense.chance);
    } else {
        return rollChance(defense.chance + (defender.stats.speed - attacker.stats.accuracy) * 2);
    }
}

function applyAccuracyVsSpeedModifier(damage, accuracy, speed) {
    return damage * (100 + (accuracy - speed) * 2) / 100
}

function applyDamageModifier(damage, modifier) {
    return damage * modifier / 100
}

function rollChance(chance) {
    return randomNumber(100) - chance <= 0;
}

function makeAiListChoice(list) {
    let total = 0;
    list.forEach(function (item) {
        total += item.choiceChance;
    });
    let choice = randomNumber(total);

    for (let i = 0; i < list.length; i++) {
        choice -= list[i].choiceChance;
        if (choice <= 0) {
            return i;
        }
    }
}

function nextTurn() {
    let characters = battle.characters;

    //each turn status effects
    characters.forEach(function (character) {
        resolveDamagePerTurn(character);
    });

    battle.turnIndex++;
    if (battle.turnIndex === characters.length) {
        battle.turnIndex = 0;
    }
    if (characters[battle.turnIndex].statusEffects.knockedDown) {
        characters[battle.turnIndex].statusEffects.knockedDown = false;
        nextTurn();
    }
}

function resolveDamagePerTurn(character) {
    character.statusEffects.damagePerTurns.forEach(function (damagePerTurn) {
        battle.outputText += newline + character.name + " took " + damagePerTurn.type + " damage";
        resolveDamageTaken(character, damagePerTurn.damage);
        damagePerTurn.turns--;
        if (damagePerTurn.turns === 0) {
            let index = character.statusEffects.damagePerTurns.indexOf(damagePerTurn);
            character.statusEffects.damagePerTurns.splice(index, 1);
        }
    });
}

function randomNumber(max) {
    return Math.floor(Math.random() * max) + 1;
}